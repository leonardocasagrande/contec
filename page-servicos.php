<?php get_header(); ?>

<section class="container-fluid bg-div">
  <div class="container">
    <div class="row py-3">
      <h1 class="h1-format-prep pt-5">Serviços<span class="doc-span-orange">.</span></h1>
      <div class="line-2 mt-5 mt-md-3 float-right"></div>
    </div>
  </div>
</section>

<section>
  <div class="container my-5">
    <div class="row">
      <div class="col-md-6">
        <div class="col-md-12 shadow padrao-altura">
          <div class="row bg-orange height-custom py-3 justify-content-center mt-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/contabilidade.png" class="img-fluid" alt="Imagem card">
            <h5 class="text-white pl-3 my-auto h5-format-title">Contabilidade</h5>
          </div>
          <div class="py-4">
            <h6 class="h6-format-paragraf">A CONTABILIDADE registra e interpreta os fenômenos que afetam o patrimônio das entidades, dentro dos Padrões internacionais atualmente aceitos.</h6>
            <p class="p-format-servicos pt-3">Classificação e escrituração de acordo com os princípios contábeis vigentes; apuração de balancetes, balanços anuais e periódicos, demonstrações administrativas e financeiras.</p>
            <p class="p-format-servicos pt-4">A Terceirização da contabilidade para uma equipe de especialistas com foco na atualização das melhores práticas contábeis, garante eficiência nos processos, o que permite que nosso cliente tenha segurança para se concentrar em sua atividade principal.</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="col-md-12 shadow padrao-altura">
          <div class="row bg-orange height-custom py-3 justify-content-center mt-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/fiscal.png" class="img-fluid" alt="Imagem card">
            <h5 class="text-white pl-3 my-auto h5-format-title">Fiscal</h5>
          </div>
          <div class="py-4">
            <h6 class="h6-format-paragraf">Escrituração digital dos registros fiscais e apuração dos tributos.</h6>
            <p class="p-format-servicos pt-2">Entrega das obrigações acessórias; atendimento da legislação tributária vigente e demais exigências previstas em atos normativos.</p>
            <p class="p-format-servicos py-4">A Contec conta com integração de sistemas que visa otimizar a importação dos arquivos EFD Fiscal e EFD Contribuições ou XMLs das notas e conhecimentos eletrônicos de saídas e entradas.</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="col-md-12 shadow padrao-altura">
          <div class="row bg-orange height-custom py-3 justify-content-center mt-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/trabalhista.png" class="img-fluid pr-3" alt="Imagem card">
            <h5 class="text-white my-auto h5-format-title">Pessoal e trabalhista</h5>
          </div>
          <div class="py-5">
            <h6 class="h6-format-paragraf">Equipe qualificada para prestação de serviços multiempresa.</h6>
            <p class="p-format-servicos pt-2">A Contec utiliza-se do registro informatizado de empregados, aprimorando a demanda e dispensando o tradicional sistema de livros ou fichas.</p>
            <p class="p-format-servicos pt-2">Além de observar e cumprir toda a legislação trabalhista e previdenciária de cada funcionário da empresa, a Contec analisa e aplica integralmente os acordos, convenções e dissídios coletivos de trabalho para cada categoria econômica.</p>
            <p class="p-format-servicos py-4">O sistema é preparado para as novas exigências legais, principalmente a do E-Social mediante a integração de dados.</p>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="col-md-12 shadow padrao-altura">
          <div class="row bg-orange height-custom py-4 justify-content-center mt-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/diploma.png" class="img-fluid" alt="Imagem card">
            <h5 class="text-white pl-3 my-auto h5-format-title">Legalização</h5>
          </div>
          <div class="py-5">
            <h6 class="h6-format-paragraf">Regulamentação e assessoria na abertura e dia a dia de sociedades.</h6>
            <ul class="list-none  py-3" id="list-bullets">
              <li class="p-format-servicos pb-2">Abertura de empresas, encerramento E alterações contratuais.</li>
              <li class="p-format-servicos pb-2">Estudos de viabilização e implementação de reestruturação societária</li>
              <li class="p-format-servicos pb-2">Implementação de Holding e Empresa Administradora de Bens.</li>
              <li class="p-format-servicos pb-2">Assessoria para reorganização de empresa</li>
              <li class="p-format-servicos pb-2">Controle e obtenção de Certidões Negativas.</li>
              <li class="p-format-servicos pb-2">Parcelamento de débitos fiscais.</li>
              <li class="p-format-servicos">Regulamentação junto aos Órgãos de Classe.</li>
            </ul>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="col-md-12 shadow padrao-altura">
          <div class="row bg-orange height-custom py-4 justify-content-center mt-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bpo-financeiro.png" class="img-fluid" alt="Imagem card">
            <h5 class="text-white pl-3 my-auto h5-format-title">BPO Financeiro</h5>
          </div>
          <div class="py-5">
            <h6 class="h6-format-paragraf">Você impulsiona a sua gestão e acaba com a burocracia.</h6>
            <ul id="list-bullets" class="list-none pt-3 mb-0 ">
              <li class="p-format-servicos pb-2">Fluxo de caixa</li>
              <li class="p-format-servicos pb-2">Conciliação inteligente</li>
              <li class="p-format-servicos pb-2">Relatórios gerenciais</li>
              <li class="p-format-servicos pb-2">Emissão NF-S*</li>
              <li class="p-format-servicos pb-2">Integrações com a contabilidade e outros sistemas</li>
            </ul>
            <span class="mini-li">*condições sujeitas a análise</span>

            <p class="p-format-servicos pt-2 py-4">Os serviços de BPO Financeiro são perfeitos para empresários como você, que deseja ter total controle sobre os seus números, mas que não pode se preocupar com a gestão financeira de forma direta.</p>
          </div>
        </div>
      </div>


      <div class="col-md-6">
        <div class="col-md-12 shadow padrao-altura">
          <div class="row bg-orange height-custom py-4 justify-content-center mt-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/compass.png" class="img-fluid pr-3" alt="Imagem card">
            <h5 class="text-white my-auto h5-format-title">Consultoria tributária</h5>
          </div>
          <div class="py-5">
            <h6 class="h6-format-paragraf">Consultoria completa para manter a saúde fiscal de seu modelo de negócio.</h6>
            <ul class="list-none py-3" id="list-bullets">
              <li class="p-format-servicos pb-2">Consultoria completa para manter a saúde fiscal de seu modelo de negócio. </li>
              <li class="p-format-servicos pb-2">Elaboração de relatórios, diagnósticos, estudos tributários, pareceres técnicos, projetos de consultoria, análise de
                demonstrativos financeiros;</li>
              <li class="p-format-servicos pb-2">Suporte na implantação de sistemas de gestão;</li>
              <li class="p-format-servicos pb-2">Consultoria Preventiva;</li>
              <li class="p-format-servicos">Identificação de benefícios legais e contingências fiscais.</li>
            </ul>
          </div>
        </div>
      </div>


      <div class="col-md-6">
        <div class="col-md-12 shadow padrao-altura">
          <div class="row bg-orange height-custom py-4 justify-content-center mt-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/lion.png" class="img-fluid" alt="Imagem card">
            <h5 class="text-white pl-3 my-auto h5-format-title">Imposto de renda <br> para pessoa física</h5>
          </div>
          <div class="py-5">
            <h6 class="h6-format-paragraf">Equipe especializada para elaboração da Declaração de Ajuste Anual do Imposto de Renda de Pessoa Física (DIRPF).</h6>
            <p class="p-format-servicos pt-2 py-4">A assessoria Contec oferece orientação legal para entrega dessa obrigação fiscal, incluindo a elaboração de Declaração de Espólio, Declaração Final de Espólio, Declaração de Saída Definitiva do País, Carnê Leão, Ganho de Capital, dentre outras declarações atinentes à pessoa física.</p>
          </div>
        </div>
      </div>


      <div class="col-md-6">
        <div class="col-md-12 shadow padrao-altura">
          <div class="row bg-orange height-custom py-4 justify-content-center mt-3">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cert-digital.png" class="img-fluid pr-3" alt="Imagem card">
            <h5 class="text-white my-auto h5-format-title">Certificação Digital</h5>
          </div>
          <div class="py-5">
            <h6 class="h6-format-paragraf">Tecnologia de criptografia de dados que garante a autenticidade, confidencialidade, integridade e não repúdio às informações eletrônicas.</h6>
            <ul class="list-none py-3" id="list-bullets">
              <li class="p-format-servicos pb-2">E-CNPJ A1 e A3;</li>
              <li class="p-format-servicos pb-2">E-CPF - A1 e A3.</li>

            </ul>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<section>
  <div class="container my-5">
    <div class="line-2 my-5"></div>
    <div class="row py-5 justify-content-center">
      <h1 class="text-uppercase h1-format-area">Área de busca</h1>
    </div>
    <div class="row">
      <div class="col-md-4">
        <a class="btn btn-outline-busca text-uppercase btn-block" target="_blank" href="http://www.receita.fazenda.gov.br/PessoaJuridica/CNPJ/cnpjreva/Cnpjreva_Solicitacao.asp">Consulta de cnpj</a>
        <div class="col-md-12">
          <p class="p-format-busca py-3">Andamento de novos <br> e status de antigos</p>
          <div class="line-4 float-right mt-n5"></div>
        </div>
      </div>

      <div class="col-md-4">
        <a class="btn btn-outline-busca text-uppercase btn-block" target="_blank" href="https://servicos.receita.fazenda.gov.br/Servicos/CPF/ConsultaSituacao/ConsultaPublica.asp">Consulta de cpf</a>
        <div class="col-md-12">
          <p class="p-format-busca py-3">Andamento de novos <br> e status de antigos</p>
          <div class="line-4 float-right mt-n5"></div>
        </div>
      </div>

      <div class="col-md-4">
        <a class="btn btn-outline-busca text-uppercase btn-block" target="_blank" href="http://servicos.receita.fazenda.gov.br/Servicos/consrest/Atual.app/paginas/mobile/restituicaoMobi.asp">Irpf</a>
        <div class="col-md-12">
          <p class="p-format-busca py-3">Consulta de restrições <br> de imposto de renda</p>
          <div class="line-4 float-right mt-n5"></div>
        </div>
      </div>

      <div class="col-md-4 pt-3">
        <a class="btn btn-outline-busca text-uppercase btn-block" target="_blank" href="http://receita.economia.gov.br/orientacao/tributaria/pagamentos-e-parcelamentos/taxa-de-juros-selic#Taxa_de_Juros_Selic">Selic</a>
        <div class="col-md-12">
          <p class="p-format-busca py-3">Tabela de juros de <br> taxa SELIC</p>
          <div class="line-4 float-right mt-n5"></div>
        </div>
      </div>

      <div class="col-md-4 pt-3">
        <a class="btn btn-outline-busca text-uppercase btn-block" target="_blank" href="http://www.sintegra.gov.br/">Sintegra</a>
        <div class="col-md-12">
          <p class="p-format-busca py-3">Integração de <br>informação sobre ICMS</p>
          <div class="line-4 float-right mt-n5"></div>
        </div>
      </div>

      <div class="col-md-4 pt-3">
        <a class="btn btn-outline-busca text-uppercase btn-block" target="_blank" href="http://www8.receita.fazenda.gov.br/SimplesNacional/aplicacoes.aspx?id=21">Simples nacional</a>
        <div class="col-md-12">
          <p class="p-format-busca py-3">Consulta de optantes <br> do Simples Nacional</p>
          <div class="line-4 float-right mt-n5"></div>
        </div>
      </div>

      <!-- <div class="col-md-4 pt-3">
            <a class="btn btn-outline-busca text-uppercase btn-block" href="#">Gojur</a>
            <div class="col-md-12">
              <p class="p-format-busca py-3">Acesso a processos</p>
              <div class="line-4 float-right mt-n4"></div>
            </div>
          </div>

          <div class="col-md-4 pt-3">
            <a class="btn btn-outline-busca text-uppercase btn-block" href="#">Ponto web</a>
            <div class="col-md-12">
              <p class="p-format-busca py-3">Marcação de ponto <br> web</p>
              <div class="line-4 float-right mt-n5"></div>
            </div>
          </div> -->

      <div class="position-absolute format-position">
        <div class="col-md-8 p-0 format-col-3">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/caminho.png" class="img-fluid d-none d-md-block" alt="Img absolute">
        </div>
      </div>
    </div>
  </div>
</section>

<section class="bg-gray">
  <div class="container py-5">
    <div class="row py-5">
      <div class="col-md-4">
        <h1 class="h1-format-agende-2 my-md-3 my-2">Agende uma visita <span class="doc-span-orange">.</span></h1>
      </div>
      <div class="col-md-4">
        <a class="btn btn-outline-busca my-md-3 my-2 btn-block" href="tel:+551938731115" target="_blank"><i class="fas fa-phone mr-2 icon-font"></i>19 3873.1115</a>
      </div>
      <div class="col-md-4">
        <a class="btn btn-outline-whats my-md-3 my-2 btn-block" href="https://api.whatsapp.com/send?phone=5519996978350" target="_blank"><i class="fab fa-whatsapp mr-2 icon-font"></i>19 99697-8350</a>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>