<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-149300195-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-149300195-1');
  </script>


  <script type='text/javascript'>
    window.smartlook || (function(d) {
      var o = smartlook = function() {
          o.api.push(arguments)
        },
        h = d.getElementsByTagName('head')[0];
      var c = d.createElement('script');
      o.api = new Array();
      c.async = true;
      c.type = 'text/javascript';
      c.charset = 'utf-8';
      c.src = 'https://rec.smartlook.com/recorder.js';
      h.appendChild(c);
    })(document);
    smartlook('init', '886f6f58fe9af999f1fecaf515b4cd0fb713acf7');
  </script>

  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Contabil Contec</title>
  <!-- Css Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <!-- Css Custom -->
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
  <!-- Font-awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <!-- Google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700,900&display=swap" rel="stylesheet">
  <!-- Owl carousel -->
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/owl.theme.default.min.css">
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico">
  <?php wp_head(); ?>
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-nav fixed-top pt-3">
      <div class="container">
        <div class="m-auto">
          <a href="http://www.contabilcontec.com.br/">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-contec.png" class="navbar-brand img-fluid logo-img" alt="Logo da Contec">
          </a>
        </div>
        <button class="navbar-toggler m-auto" type="button" data-toggle="collapse" data-target="#conteudoNavbarSuportado" aria-controls="conteudoNavbarSuportado" aria-expanded="false" aria-label="Alterna navegação">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="conteudoNavbarSuportado">
          <div class="row bg-nav-white m-auto">
            <ul class="navbar-nav m-auto">
              <li class="nav-item ml-md-5 mr-sm-3">
                <a class="nav-link li-header" href="<?php echo get_site_url() ?>/historia">História</a>
              </li>
              <li class="nav-item mr-sm-3 ">
                <a class="nav-link li-header" href="<?php echo get_site_url() ?>/servicos">Serviços</a>
              </li>
              <li class="nav-item mr-sm-3 ">
                <a class="nav-link li-header" href="<?php echo get_site_url() ?>/qualidade">Qualidade</a>
              </li>
              <!-- <li class="nav-item mr-sm-3 ">
                <a class="nav-link li-header" href="<?php echo get_site_url() ?>/contato">Contato</a>
              </li> -->
              <li class="nav-item mr-sm-3">
                <a class="nav-link li-header" href="https://onvio.com.br/clientcenter/pt/auth?r=%2Fhome"><i class="fas fa-sign-out-alt mr-1"></i>Área restrita</a>
              </li>
              <!-- <a href="tel:+551938731115" class="btn btn-outline-fale text-uppercase">Fale conosco</a> -->
              <div class="dropdown">
                <button class="btn btn-outline-fale dropdown-toggle text-uppercase" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Fale conosco
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a class="dropdown-item li-header" href="<?php echo get_site_url() ?>/contato">Contato</a>
                  <!-- <a class="dropdown-item" href="mailto:contec@contabilcontec.com.br?subject=Questions">E-mail</a> -->
                  <a class="dropdown-item li-header" target="_blank" href="https://api.whatsapp.com/send?phone=5519996978350">19 99697-8350</a>
                </div>
              </div>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  </header>

  <?php

  function paginaPrincipal()
  {
    $bg_class = '';
    if (is_front_page()) {
      $bg_class = 'bg-div-home';
    } else {
      $bg_class = 'bg-div';
    }
    return $bg_class;
  }

  ?>
  <div class="d-none d-md-block <?php echo paginaPrincipal() ?>"></div>