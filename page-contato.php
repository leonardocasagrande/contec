<?php get_header(); ?>

<section class="container-fluid bg-div">
  <div class="container">
    <div class="row py-3">
      <div class="col-md-6 pt-3">
        <h1 class="h1-format-prep pt-5">Contato<span class="doc-span-orange">.</span></h1>
        <div class="line-2 mt-3 mt-md-2 float-right"></div>
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container py-5">
    <div class="row">
      <div class="col-md-6">
        <div class="line-5 my-3 mr-2 float-left d-none d-md-block"></div>
        <h1 class="h1-format-contato pt-1">Some com a gente também!</h1>
        <p class="p-format-contato mt-2 ml-3">Agende uma consultoria gratuita agora mesmo e conheça um pouco mais do que podemos fazer pelo dia a dia de sua empresa.</p>
      </div>
      <div class="col-md-6 pt-sm-8 pt-7">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.508447214787!2d-47.27321688441995!3d-22.820671040542518!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c8bd699076965b%3A0x2a257c33612d1d!2sR.+M%C3%A1ximo+Biondo%2C+492+-+Centro%2C+Sumar%C3%A9+-+SP%2C+13170-029!5e0!3m2!1spt-BR!2sbr!4v1561378771253!5m2!1spt-BR!2sbr" class="mapa-format mt-n7" frameborder="0" allowfullscreen></iframe>
      </div>
      <div class="col-md-6 mt-n6 mt-sm-n6">
        <?= do_shortcode('[contact-form-7 id="22" title="Formulário de contato 1"]'); ?>
      </div>
      <div class="col-md-3 pt-3 text-sm-left text-center">
        <p class="p-format-page-contato text-uppercase">Suporte</p>
        <div class="div-format">Fone:</div>
        <a href="https://api.whatsapp.com/send?phone=551.999697835019997e+21" target="_blank" class="tele-format"><i class="fab fa-whatsapp" style="margin-right: 10px; color: #25D366;"></i>19 99697.8350</a>
        <div class="pt-4 div-format">E-mail:</div>
        <a href="mailto:contec@contabilcontec.com.br?subject=Questions" class="email-format">contec@contabilcontec.com.br</a>
      </div>
      <div class="col-md-3 pt-3 text-sm-left text-center">
        <p class="p-format-page-contato text-uppercase">Endereço</p>
        <ul class="list-unstyled sumare-format">
          <li><strong>Sumaré · SP</strong></li>
          <li>Rua Máximo Biondo, 492</li>
          <li>Centro - CEP 13170-029</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="position-absolute format-position">
    <div class="col-md-8 m-0 p-0 format-col mt-n6">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/caminho-contato.png" class="img-fluid d-none d-md-block" alt="Img absolute">
    </div>
  </div>
  </div>
</section>

<?php get_footer(); ?>