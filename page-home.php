<?php get_header(); ?>

<!-- <div class="fundo">
    <div class="popup">
      <a href="https://api.whatsapp.com/send?phone=5519992680768"><img class="img-fluid " src="http://www.contabilcontec.com.br/wp-content/themes/Contec/img/popup.png?_t=1584109100" alt=""></a>
      <a href="#"><i class="icon-close far fa-times-circle fa-2x"></i></a>
    </div>
  </div>
    -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-lg-3 mr-lg-5 pr-lg-5 pt-5">
        <h2 class="h2-format-nova text-md-left text-center text-uppercase pt-3">Contec</h2>
        <!-- <h1 class="h1-format-prep text-md-left text-center">Preparada para os desafios da sua empresa<span class="doc-span-orange">.</span></h1> -->
        <h1 class="banner-text-custom ">uma história <span class='grande'>con</br>tada</br></span> por conquistas<span class="doc-span-orange">.</span>
        </h1>
        <a class="btn btn-danger py-2 w-100 mt-5 mt-lg-3 d-none d-md-block" href="<?php echo get_site_url() ?>/historia">Conheça a Contec</a>
      </div>

      <div class="col-md-8 p-0 mb-06">
        <div class="owl-carousel owl-theme " id="carousel1">
          <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg-home-50.png" class="img-fluid" alt="Imagem Home">
          </div>
          <!-- <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg-home-1.jpg" class="img-fluid" alt="Imagem Home">
          </div>
          <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg-home-2.jpg" class="img-fluid" alt="Imagem Home">
          </div>
          <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg-home-3.jpg" class="img-fluid" alt="Imagem Home">
          </div> -->
        </div>
        <div class="col-8 m-auto">
          <a class="btn btn-danger btn-block d-md-none d-sm-block btn-owlcarousel" href="<?php echo get_site_url() ?>/historia">Conheça a Contec</a>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="container-fluid bg-orange">
  <div class="container py-5">
    <div class="row">
      <div class="col-md-6">
        <h1 class="text-white h1-format-contec">Contec em números<span class="doc-span-black">.</span></h1>
      </div>
      <div class="col-md-6">
        <div class="line mt-5 mt-md-3 d-none d-sm-block float-right"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 pt-3">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-colaboradores.png" class="float-left" alt="Icone Colaboradores">

        <p class="text-white ml-md-5 ml-5 p-format">Contec 50 anos </br>e expertise nos mais diversos assuntos pertinentes ao dia a dia de sua empresa.</p>
      </div>

      <div class="col-md-4 pt-3">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-nova-sede.png" class="float-left" alt="Icone Sede própria">
        <img class="img-fluid ml-2 img-width" src="<?php echo get_stylesheet_directory_uri(); ?>/img/30-colaboradores.png" alt="30 Colaboradores">
        <p class="text-white ml-md-5 ml-5 p-format">especialistas em diversas áreas da contabilidade a sua dispositivo.</p>
      </div>

      <div class="col-md-4 pt-3">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icon-tradicao.png" class="float-left" alt="Icone Tradição">
        <!-- <img class="img-fluid ml-3 img-width" src="<?php echo get_stylesheet_directory_uri(); ?>/img/nova-sede.png" alt="Nova sede"> -->

        <p class="text-white ml-md-5 mb-0 ml-5 p-format">Sede própria com 400m²</p>
        <p class="text-white ml-md-5 ml-5 p-format">e uma estrutura completa para proporcionar um ambiente colaborativo e produtivo.</p>
      </div>
    </div>
</section>
<section>
  <div class="container my-5 py-4">
    <div class="row">
      <div class="col-md-6">
        <h1 class="h1-format-estru">Estrutura<span class="doc-span-orange">.</span></h1>
        <p class="p-format-estru">Um novo espaço, totalmente dedicado a você.</p>
      </div>
      <div class="col-md-6">
        <div class="line-2 mt-5 mt-md-3 d-none d-sm-block float-right"></div>
      </div>
    </div>
    <div class="col-md-12 m-auto pt-3">
      <div class="owl-carousel carousel-estrutura owl-theme " id="carousel2">

        <!-- <div class="item i-1"></div> -->
        <!-- <div class="item i-2"></div> -->
        <!-- <div class="item i-3"></div> -->
        <!-- <div class="item i-4"></div> -->
        <!-- <div class="item i-5"></div> -->
        <!-- <div class="item i-6"></div> -->
        <!-- <div class="item i-7"></div> -->
        <!-- <div class="item i-8"></div> -->
        <!-- <div class="item i-9"></div> -->
        <!-- <div class="item i-10"></div> -->
        <!-- <div class="item i-11"></div> -->
        <!-- <div class="item i-12"></div> -->
        <div class="item i-13"></div>
        <div class="item i-14"></div>
        <div class="item i-15"></div>
        <div class="item i-16"></div>
        <div class="item i-17"></div>
        <div class="item i-18"></div>
        <div class="item i-19"></div>
        <div class="item i-20"></div>
        <div class="item i-21"></div>
        <div class="item i-22"></div>
        <div class="item i-23"></div>
        <div class="item i-24"></div>
        <div class="item i-25"></div>
        <div class="item i-26"></div>
        <!-- <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-1.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-2.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-3.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-4.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-5.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-6.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-7.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-8.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-9.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/estrutura-image-10.jpg" class="img-fluid" alt="Imagem Estrutura">
        </div> -->


      </div>
    </div>
    <div class="col-md-12 mt-md-4 text-center">
      <span class="customPrevBtn mt-n4-5 mr-5 noselect">&larr;</span>
      <span class="customNextBtn mt-n4-5 ml-n4 noselect">&rarr;</span>
    </div>
  </div>
</section>
<section>
  <div class="container py-3">
    <div class="row">
      <div class="col-md-3">
        <h3 class="h3-format text-md-center text-left">Por que <span class="doc-span-orange"> Contec</span>?</h3>
      </div>
      <div class="col-md-9">
        <div class="owl-carousel owl-theme" id="carousel3">
          <div class="item my-2">
            <div class="card card-home m-auto">
              <div class="card-body py-4 px-3">
                <img class="card-img-top-1 img-fluid mb-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/atendimento.png" alt="Imagem de capa do card">
                <h5 class="card-title h5-format-card">Atendimento personalizado</h5>
                <p class="card-text p-format-card">Cada empresa tem a sua necessidade e a Contec está disponível para atender as suas.</p>
              </div>
            </div>
          </div>


          <div class="item my-2">
            <div class="card card-home m-auto">
              <div class="card-body py-4 px-3">
                <img class="card-img-top-1 img-fluid mb-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/seriedade.png" alt="Imagem de capa do card">
                <h5 class="card-title h5-format-card">Seriedade comprovada</h5>
                <p class="card-text p-format-card">Dedicação total ao tratar de assuntos contibeis, fiscais, trabalhistas e legais de sua empresa.</p>
              </div>
            </div>
          </div>

          <div class="item my-2">
            <div class="card card-home m-auto">
              <div class="card-body py-4 px-3">
                <img class="card-img-top-1 img-fluid mb-3" src="<?php echo get_stylesheet_directory_uri(); ?>/img/experiencia.png" alt="Imagem de capa do card">
                <h5 class="card-title h5-format-card">Experiência que agrega valor</h5>
                <p class="card-text p-format-card">São 50 anos de mercado que somaram muito no know-how da Contec.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="line-3 my-5 d-none d-sm-block float-right"></div>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/circle-contec.png" class="img-fluid img-fluando d-none d-md-block" alt="Cirle Image">
    </div>
  </div>
</section>
<!-- 
<section>
  <div class="container my-3 py-4">
    <div class="row m-auto align-items-center">
      
      <div class="col-md-6  mt-4">
        <h1 class="h1-format-estru text-md-left text-center col-md-8">Qualidade técnica certificada<span class="doc-span-orange">.</span></h1>
      </div>
      <div class="col-md-6 pt-3 mt-2">
        <p class="p-format-iso">A qualidade da Contec Contabilidade é resultante de um processo interno baseado nas ABNT NBR ISO e a ferramenta de qualidade 5S.</p>
      </div>
    </div>
  </div>
</section> -->

<section class="container-fluid bg-gray">
  <div class="container py-5 my-3">
    <div class="row">
      <div class="col-md-5">
        <p class="p-format-orange">Política de qualidade<span class="doc-span-black">.</span></p>
        <p class="p-black pb-5">A Contec Contabilidade através do comprometimento da Alta Direito e de sua Equipe busca:</p>
      </div>
      <div class="col-md-7">
        <ul class="bg-list py-5 position-md-absolute  position-absolute px-4" id="list-bullets">
          <li class="text-white li-format pb-3">Melhorar continuamente os serviços prestados, respeitando os requisitos, dentro dos princípios éticos e legais, visando a satisfação dos nossos clientes;</li>
          <li class="text-white li-format pb-3">Atuar na melhoria contínua do sistema de gestão da qualidade, através da busca constante de suas metas;</li>
          <li class="text-white li-format pb-3">Investir na capacitação e atualização de nossos funcionários através de treinamentos;</li>
          <li class="text-white li-format pb-3">Estar comprometida com as expectativas do cliente, dos profissionais e da empresa.</li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="pt-md-5">
  <div class="container pt-md-5">
    <div class="row pt-6 pt-md-6">
      <div class="col-md-4 py-md-5">
        <h3 class="h3-format">Acompanhe o
          que nossos clientes
          dizem sobre nós<strong class="span-color">.</strong></h3>
      </div>
      <div class="col-md-8">
        <div class="owl-carousel owl-theme" id="carousel6">
          <div class="item my-2">
            <div class="m-auto">
              <div class="card-body py-4 px-3">
                <img class="card-img-top-2 img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/aspas.png" alt="Imagem de capa do card">
                <!-- <h5 class="card-title h5-format-card">Atendimento personalizado</h5> -->
                <p class="card-text p-format-card">A CONTEC faz parte de uma parceria de sucesso com a FMC há 10 anos, atuando nos processos de apurações de impostos indiretos e na entrega de obrigações acessorias ao fisco nos Ambitos Estadual, Federal e Municipal. Tudo isso considerando sempre as exigências da legislação vigente. Esse é um projeto que depende do empenho de cada um dos membros de equipe da CONTEC, e que merece nosso sincero agradecimento pelo trabalho realizado! Parabéns e obrigado, equipe!</p>
              </div>
              <div class="px-3">
                <span class="text-left text-format-autor">Elizete Dias</span><br>
                <span class="text-uppercase text-format-sub">FMC QUIMICA DO BRASIL LTDA</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<section class="my-5 mx-3">
  <div class="container my-5 bg-gray">
    <div class="row py-5">
      <div class="col-md-4 my-auto">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/area-restrita.png" class="img-fluid float-md-right rounded mx-auto d-block" alt="脕rea restrita">
      </div>
      <div class="col-md-4 text-md-left text-center">
        <h1 class="h1-format-area mt-4">Área Restrita<span class="doc-span-black">.</span></h1>
        <p class="p-format-area">Acesse e acompanhe a contabilidade de sua empresa.</p>
      </div>
      <div class="col-md-4 text-md-left text-center my-auto">
        <a href="https://onvio.com.br/clientcenter/pt/auth?r=%2Fhome" class="btn btn-outline-acesse text-uppercase px-5">Acessar</a>
      </div>
    </div>
    <div class="position-absolute format-position">
      <div class="col-md-8 p-0 format-col">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/caminho.png" class="img-fluid d-none d-md-block" alt="Img absolute">
      </div>
    </div>
  </div>
</section>
<section class="container-fluid bg-orange py-5">
  <div class="container">
    <div class="row py-5">
      <div class="col-md-5">
        <h1 class="h1-format-agende">Agende uma visita<span class="doc-span-black">.</span></h1>
        <p class="p-format-agende">Venha somar com a gente, entre em contato por:</p>
      </div>
      <div class="col-md-7">
        <div class="row">
          <div class="col-md-6">
            <a class="btn btn-outline-agende my-md-3 my-2 btn-block" href="tel:+551938731115" target="_blank"><i class="fas fa-phone mr-2 icon-font"></i>19 3873.1115</a>
          </div>
          <div class="col-md-6">
            <a class="btn btn-outline-agende my-md-3 my-2 btn-block" href="https://api.whatsapp.com/send?phone=5519996978350" target="_blank"><i class="fab fa-whatsapp mr-2 icon-font"></i>19 99697-8350</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<?php get_footer(); ?>