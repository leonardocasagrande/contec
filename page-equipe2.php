<?php get_header(); ?>


<section class="container-fluid bg-div">
  <div class="container">
    <div class="row py-3">
      <h1 class="h1-format-prep pt-5">Equipe<span class="doc-span-orange">.</span></h1>
      <div class="line-2 mt-5 mt-md-3 float-right"></div>
    </div>
  </div>
</section>

<section>
  <div class="container">
    <div class="row px-3">
      <div class="line-6 ml-md-n4 mt-6 mr-2 float-left d-none d-md-block"></div>
      <h1 class="h1-format-equi pt-5">Uma equipe totalmente disponível para você</h1>
    </div>
    <p class="p-format-equi pt-3">São mais de 30 especialistas em atender as necessidades <br> da sua empresa. Conheça a equipe Contec:</p>
  </div>
</section>

<section class="pt-5">
  <div class="position-absolute format-position-3 pt-3">
    <div class="col-md-8 m-0 p-0 format-col">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/triangulo-lateral.png" class="img-fluid" alt="Img absolute">
    </div>
  </div>
  <div class="container">
    <div class="row mx-auto">
      <div class="owl-carousel owl-theme" id="carousel5">

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/andresa-hoffman.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Andresa <br> Hoffman</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Diretora</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/francinette-miranda.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Francinette <br> Miranda</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Diretora</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/andrea-cestari.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Andreia <br> Cestari</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Contabilidade</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/camila-melo.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Camila <br> Melo</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Departamento Fiscal</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/carol.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Carolina <br> Varquez</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Contabilidade</h6>
            </div>
          </div>
        </div>


        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/catia.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Catia <br> Regina</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Departamento Pessoal</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/danielle-delmonte.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Danielle <br> Delmonte</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Departamento Fiscal</h6>
            </div>
          </div>
        </div>
        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/gabriela-bianco.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Gabriela <br> Bianco</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Contabilidade</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/liliane-oliveira.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Liliane <br> Oliveira</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Contabilidade</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/mercia-dos-santos.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Mércia dos <br> Santos</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Contabilidade</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/naiara-do-prado.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Naiara do <br> Prado</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Societario</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/patricia-rosa.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Patricia <br> Rosa</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Contabilidade</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/poliana-da-silva.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Poliana da <br> Silva</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Financeiro</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/priscila-aparecida.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Priscila <br> Aparecida</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Departamento Fiscal</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/rafaela-tomazin.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Rafaela <br> Tomazin</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Administração</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/rayssa-cavalcante.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Rayssa <br> Cavalcante</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Departamento Fiscal</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/silvana-araujo.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Silvana de <br> Araújo</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Departamento Fiscal</h6>
            </div>
          </div>
        </div>

        <div class="item my-2">
          <div class="card card-equipe mx-auto">
            <img class="card-img-top" src="<?php echo get_stylesheet_directory_uri(); ?>/img/vanessa-assis.png" class="img-fluid" alt="Imagem de capa do card">
            <div class="card-body bg-black">
              <h5 class="card-title-h5 text-white">Vanessa <br> Assis</h5>
              <div class="line-4 float-right mt-n5"></div>
              <h6 class="card-subtitle-h6 mb-2 text-muted text-uppercase">Contabilidade</h6>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="col-md-12 mt-md-2 text-center">
      <span class="customPrevBtn mt-n4-5 mr-5 noselect">&larr;</span>
      <span class="customNextBtn mt-n4-5 ml-n4 noselect">&rarr;</span>
    </div>
  </div>
</section>
<section class="my-5 mx-3">
  <div class="container bg-orange">
    <div class="row py-5 px-5">
      <div class="col-md-8 my-auto">
        <h1 class="text-white h1-format-agende">Venha somar com a gente<span class="doc-span-black">.</span></h1>
        <p class="text-white p-format-area">Agende uma reunião agora mesmo e conheça um pouco mais do que podemos fazer pelo dia a dia da sua empresa.</p>
      </div>
      <div class="col-md-4 m-auto">
        <a class="btn btn-outline-agendar my-md-3 my-2 btn-block" href="mailto:contec@contabilcontec.com.br?subject=Questions">Agendar</a>
      </div>
    </div>
  </div>
</section>


<?php get_footer(); ?>
