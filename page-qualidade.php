<?php get_header(); ?>

<section class="container-fluid bg-div">
  <div class="container">
    <div class="row py-3">
      <h1 class="h1-format-prep pt-5">Qualidade<span class="doc-span-orange">.</span></h1>
      <div class="line-2 mt-5 mt-md-3 float-right"></div>
    </div>
  </div>
</section>

<!-- <section>
  <div class="container">
    <div class="row py-5 align-items-center">
      <div class="col-md-8 text-center" style="margin: 0 auto;">
        <p class="p-format-qualidade my-auto">A constante dedicação em oferecer os melhores serviços e processos <br> para cada um de seus parceiros trouxe certificações de grande <br> expressão para a Contec, creditando toda sua eficiência e exaltando <br> toda a qualidade que a empresa dispõe.</p>
      </div>
    
    </div>
  </div>
</section> -->

<section class="container-fluid bg-gray">
  <div class="container py-5 mb-3">
    <div class="row">
      <div class="col-md-5">
        <p class="p-format-orange">Política de qualidade <span class="doc-span-black">.</span></p>
        <p class="p-black pb-5">A Contec Contabilidade através do comprometimento da Alta Direção e de sua Equipe busca:</p>
      </div>
      <div class="col-md-7 mt-n4">
        <div class="bg-list py-5 position-md-absolute  position-absolute">
          <p class="text-white px-5 li-format">Melhorar continuamente os serviços prestados, respeitando os requisitos, dentro dos princípios éticos e legais, visando a satisfação dos nossos clientes;<br><br>
            Atuar na melhoria contínua do sistema de gestão da qualidade, através da busca constante de suas metas;<br> <br>
            Investir na capacitação e atualização de nossos funcionários através de treinamentos; <br><br>
            Estar comprometida com as expectativas do cliente, dos profissionais e da empresa.</p>
        </div>
        </ul>
      </div>
    </div>
  </div>
</section>

<section class="my-6 mx-3">
  <div class="container bg-orange">
    <div class="row py-5 px-5">
      <div class="col-md-8 my-auto">
        <h1 class="text-white h1-format-agende">Venha somar com a gente<span class="doc-span-black">.</span></h1>
        <p class="text-white p-format-area">Agende uma reunião agora mesmo e conheça um pouco mais do que podemos fazer pelo dia a dia da sua empresa.</p>
      </div>
      <div class="col-md-4 m-auto">
        <a class="btn btn-outline-agendar my-md-3 my-2 btn-block" href="mailto:contec@contabilcontec.com.br?subject=Questions">Agendar</a>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>