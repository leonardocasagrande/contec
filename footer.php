<footer class="mt-5">
  <!-- Footer Desktop -->
  <div class="container d-none d-md-block">
    <div class="row">
      <div class="col-md-10">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-contec.png" class="logo-footer" alt="Logo Footer">
        <div class="d-inline-block list-footer-desktop">
          <a href="<?php echo get_site_url() ?>/historia" class="ml-5 mr-5">História</a>
          <a href="<?php echo get_site_url() ?>/servicos" class="mr-5">Serviços</a>
          <a href="<?php echo get_site_url() ?>/qualidade" class="mr-5">Qualidade</a>
          <a href="<?php echo get_site_url() ?>/contato" class="mr-5">Contato</a>
          <a href="https://onvio.com.br/clientcenter/pt/auth?r=%2Fhome"><i class="fas fa-sign-out-alt"></i>Área restrita</a>
        </div>
      </div>
      <div class="col-md-2 text-md-right">
        <a href="https://www.instagram.com/conteccontabilidad3/"><i class="fab fa-instagram py-2 pl-4 icone-color"></i></a>
        <a href="https://www.facebook.com/contabilcontec/"><i class="fab fa-facebook-f py-2 pl-4 icone-color"></i></a>
        <a href="#"><i class="fab fa-linkedin-in py-2 pl-4 icone-color"></i></a>
      </div>
    </div>
    <div class="row mt-5">
      <div class="col-9">
        <span class="span-footer">&copy 2019 Contec Contabilidade.</span>
      </div>
      <div class="col-3 text-right">
        <ul class="list-unstyled list-footer-sumare">
          <li><strong>Sumaré · SP</strong></li>
          <li>Rua Máximo Biondo, 492</li>
          <li>Centro - CEP 13170-029</li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Fim do footer desktop -->

  <!-- Footer do Mobile -->
  <div class="container d-md-none">
    <div class="row">
      <div class="col-6">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-contec.png" class="logo-footer" alt="Logo Footer">
      </div>
      <div class="col-6">
        <a href="https://www.instagram.com/conteccontabilidad3/"><i class="fab fa-instagram py-2 pl-4 icone-color"></i></a>
        <a href="https://www.facebook.com/contabilcontec/"><i class="fab fa-facebook-f py-2 pl-4 icone-color"></i></a>
        <a href="#"><i class="fab fa-linkedin-in py-2 pl-4 icone-color"></i></a>
      </div>
    </div>
  </div>
  <div class="container d-md-none">
    <div class="row">
      <div class="col-4 pt-5">
        <ul class="list-unstyled list-footer">
          <li class="pb-3"><a href="<?php echo get_site_url() ?>/historia">História</a></li>
          <li class="pb-3"><a href="<?php echo get_site_url() ?>/servicos">Serviços</a></li>
          <li class=""><a href="<?php echo get_site_url() ?>/equipe">Equipe</a></li>
        </ul>
      </div>

      <div class="col-4 pt-5">
        <ul class="list-unstyled list-footer">
          <li class="pb-3"><a href="<?php echo get_site_url() ?>/qualidade">Qualidade</a></li>
          <li class="pb-3"><a href="<?php echo get_site_url() ?>/contato">Contato</a></li>
          <li class=""><a href="https://onvio.com.br/clientcenter/pt/auth?r=%2Fhome"><i class="fas fa-sign-out-alt mr-1"></i>Área restrita</a></li>
        </ul>
      </div>

      <div class="col-4 pt-6">
        <ul class="list-unstyled list-footer-sumare">
          <li><strong>Sumaré · SP</strong></li>
          <li>Rua Máximo Biondo, 492</li>
          <li>Centro - CEP 13170-029</li>
        </ul>
      </div>
      <span class="span-footer">&copy <?php echo date("Y"); ?> Contec Contabilidade.</span>
    </div>
  </div>
  <!-- Fim do footer mobile  -->
</footer>





</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.min.js"></script>

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.6/owl.carousel.min.js"></script> -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/app.js"></script>


</html>