$("#carousel1").owlCarousel({
  loop: false, //disable
  touchDrag: false, //disable
  pullDrag: false, //disable
  freeDrag: false, //disable
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});
var owl1 = $("#carousel2");
owl1.owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  onInitialized: owl1onChange,
  onTranslated: owl1onChange,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 3,
    },
  },
});

$("#carousel3").owlCarousel({
  loop: false,
  margin: 0,
  nav: false,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 3,
    },
  },
});

var owl2 = $("#carousel4");
owl2.owlCarousel({
  loop: false,
  margin: 10,
  nav: true,
  onInitialized: owl2onChange,
  onTranslated: owl2onChange,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});

$("#carousel5").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 2,
    },
    1000: {
      items: 3,
    },
  },
});

$("#carousel6").owlCarousel({
  loop: true,
  margin: 10,
  nav: true,
  responsive: {
    0: {
      items: 1,
    },
    600: {
      items: 1,
    },
    1000: {
      items: 1,
    },
  },
});

//Scrip de barra de rolagem
$(window).scroll(function () {
  if ($(this).scrollTop() > 50) {
    $(".navbar").addClass("bg-jq-white");
  } else {
    $(".navbar").removeClass("bg-jq-white");
  }
});

// Script owl carousel2
function owl1onChange(event) {
  var element = event.target;
  var items = event.item.count;
  var item = event.item.index + 1;

  if (item > items) {
    item = item - items;
  }

  $("#counter2").html("0" + item + " / 0" + items);
}

$(".customNextBtn").click(function () {
  owl1.trigger("next.owl.carousel");
});

$(".customPrevBtn").click(function () {
  owl1.trigger("prev.owl.carousel");
});

// Script contador
function owl2onChange(event) {
  var element = event.target;
  var items = event.item.count;
  var item = event.item.index + 1;
  if (item > items) {
    item = item - items;
  }
  $("#counter").html("0" + item + " / 0" + items);
}

$(".customNextBtn").click(function () {
  owl2.trigger("next.owl.carousel");
});

$(".customPrevBtn").click(function () {
  owl2.trigger("prev.owl.carousel");
});

// Script owl carousel5
var owl3 = $("#carousel5");
owl3.owlCarousel();

$(".customNextBtn").click(function () {
  owl3.trigger("next.owl.carousel");
});

$(".customPrevBtn").click(function () {
  owl3.trigger("prev.owl.carousel");
});

$(".fundo a").on("click", function () {
  $(".fundo").remove();
});
