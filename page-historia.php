<?php get_header(); ?>

<section class="container-fluid bg-div">
  <div class="container">
    <div class="row py-3">
      <h1 class="h1-format-prep pt-5">História<span class="doc-span-orange">.</span></h1>
      <div class="line-2 mt-5 mt-md-3 float-right"></div>
    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row align-items-center pt-5">

      <div class="text-center">
        <div class="d-lg-flex justify-content-center">
          <div class="line-5 ml-md-n2 mt-6 mr-2  d-none d-md-block"></div>
          <h1 class="h1-format-his pt-5 ">50 anos de dedicação pela sua empresa</h1>
        </div>
        <p class="p-format-hist pt-3 ">Desde 1971, prestamos serviços contábeis a empresas de diferentes setores por toda a Região Metropolitana de Campinas. Durante essa trajetória construímos uma relação sólida com nossos parceiros que nos permitiu uma grande experiência e aperfeiçoamento do nosso trabalho. Desenvolvemos uma cultura de investimento em atualização e tecnologia, que nos deu uma grande capacidade de adaptação às mudanças do mercado e a certeza de oferecer a melhor assessoria, atendimento personalizado e resultados que superam expectativas.</p>

        <p class="p-format-hist color-orange pt-3"> Atualmente, contamos com uma estrutura moderna e capaz de atender clientes de todos os portes. O aperfeiçoamento constante de nosso trabalho e a inovação com a utilização de recursos tecnológicos trazem mais agilidade, precisão e eficiência aos nossos serviços.</p>

        <div class="row mx-auto py-3">
          <div class="col-md-5">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/somando-sempre.png" class="img-fluid rounded mx-auto d-block" alt="Imagem somando sempre">
          </div>
          <div class="col-md-3">
            <p class="p-format-hist text-md-left text-center">Baixe a
              apresentação institucional</p>
          </div>
          <div class="col-md-4">
            <a class="btn btn-outline-download text-uppercase btn-block" target="_blank" href="<?php echo get_site_url(); ?>/wp-content/uploads/2019/08/Apresentacao_v04.pdf">Download</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container px-0 my-3 d-lg-flex">
    <iframe class="iframe-width col-lg-6 " src="https://www.youtube.com/embed/yFzCcxMHoMg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

    <iframe class="iframe-width col-lg-6" src="https://www.youtube.com/embed/AM5lTb5hw7Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>


</section>



<section class="container-fluid bg-gray ">
  <div class="container py-5">
    <div class="row pt-5">
      <div class="owl-carousel owl-theme mb-2" id="carousel4">
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/faixada-historia-1.jpg" class="img-fluid" alt="Imagem historia">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/faixada-historia-2.jpg" class="img-fluid" alt="Imagem historia">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/faixada-historia-3.jpg" class="img-fluid" alt="Imagem historia">
        </div>
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/faixada-historia-4.jpg" class="img-fluid" alt="Imagem historia">
        </div>
        <!-- <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/faixada-historia-5.jpg" class="img-fluid" alt="Imagem historia">
          </div> -->
        <div class="item">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/faixada-historia-6.jpg" class="img-fluid" alt="Imagem historia">
        </div>


      </div>
      <span class="col-md-6 text-md-left text-center">
        <span class="contador-format" id="counter">00</span>
      </span>
      <div class="col-md-6 mt-md-4 text-center">
        <span class="customPrevBtn float-md-right mt-n4-5 mr-5 noselect">&larr;</span>
        <span class="customNextBtn float-md-right mt-n4-5 ml-n4 noselect">&rarr;</span>
      </div>
    </div>
    <div class="col-md-6 mx-auto py-5">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/aspas-dupla.png" class="img-fluid rounded mx-auto d-block position-absolute img-aspas" alt="Aspas dupla">
      <p class="p-format-cres text-center">Crescemos e evoluímos
        para melhor atendê-lo</p>
    </div>
  </div>
</section>

<section>
  <div class="container cultura-contec">
    <div class="row ">
      <div class=" bg-cultura px-5 py-5">
        <h1 class="h1-format-cul text-center text-white">Nossos princípios<span class="doc-span-black">.</span></h1>
        <!-- <p class="text-white text-center p-format-cultura">Para atender as necessidades de cada um dos nossos clientes, trabalhamos no acompanhamento das constantes alterações nas leis e normas vigentes, em todos os segmentos, permitindo que eles possam se dedicar somente ao que realmente importa: o dia a dia dos negócios. Por isso, aprimoramos constantemente nossos processos visando estabelecer uma relação de confiança e credibilidade em um mercado tão competitivo em que atuamos.</p> -->
      </div>
    </div>
  </div>
  <div class="container-fluid  bg-hist py-5 ">

    <div class="container mb-250  px-0">
      <div class="d-flex align-items-start col-lg-10 mx-auto pt-3">

        <img class="col-3 size-lg" src="<?= get_stylesheet_directory_uri(); ?>/img/icon-history1.png" alt="">

        <div>
          <h3 class="h3-format-exce text-white">O que fazemos<span class="dot">.</span></h3>
          <p class="text-white p-format-exce">Ajudamos empresários e gestores a resolverem seus problemas contábeis, fiscais e trabalhistas através de uma consultoria contábil baseada na excelência e satisfação total.</p>
        </div>

      </div>

      <div class="d-flex align-items-start col-lg-10 mx-auto pt-5">

        <img class="col-3 size-lg" src="<?= get_stylesheet_directory_uri(); ?>/img/icon-history2.png" alt="">

        <div>
          <h3 class="h3-format-exce text-white">Como fazemos<span class="dot">.</span></h3>
          <p class="text-white p-format-exce">Analisamos a melhor carga tributária para o seu tipo de negócio, assim como sua folha de pagamento, identificando riscos que você talvez nem saiba que existam, e propomos soluções que estejam de acordo com suas expectativas, procurando estar presente em seu dia a dia.</p>
        </div>

      </div>

      <div class="d-flex align-items-start col-lg-10 mx-auto pt-5">

        <img class="col-3 size-lg" src="<?= get_stylesheet_directory_uri(); ?>/img/icon-history2.png" alt="">

        <div>
          <h3 class="h3-format-exce text-white">Quem somos<span class="dot">.</span></h3>
          <p class="text-white p-format-exce">Uma empresa que nos últimos 50 anos, com um time de mais de 30 pessoas, ajuda empreendedores a reduzir sua carga tributária, traduzindo a tão complicada e densa legislação brasileira. Instalada em Sumaré desde 1971, conta com uma sede própria que, em 2018, passou por uma grande reestruturação, para melhorar o atendimento aos mais de 300 empresários que contam conosco em seu dia a dia.</p>
        </div>

      </div>

      <div class="box-blue-history  ">

        <span class="pb-5 pb-lg-2"><span class="orange">Missão:</span> acolher os empresários e pessoas, oferecendo soluções contábeis eficazes.</span>

        <span class="pb-5 pb-lg-2"><span class="orange">Visão:</span> ser referência em serviços contábeis, sendo reconhecida como uma empresa que agrega valor ao negócio de seus clientes.</span>

        <span><span class="orange">Valores:</span> comprometimento, transparência, trabalho em equipe, respeito.</span>

      </div>

    </div>
  </div>
</section>
<section class="my-5 mx-3">
  <!-- <div class="position-absolute format-position-2">
    <div class="col-md-8 m-0 p-0 format-col-2">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/caminho-2.png" class="img-fluid d-none d-md-block" alt="Img absolute">
    </div>
  </div> -->
  <div class="container bg-orange">
    <div class="row py-5 px-5">
      <div class="col-md-8 my-auto">
        <h1 class="text-white h1-format-agende">Venha somar com a gente<span class="doc-span-black">.</span></h1>
        <p class="text-white p-format-area">Agende uma reunião agora mesmo e conheça um pouco mais do que podemos fazer pelo dia a dia da sua empresa.</p>
      </div>
      <div class="col-md-4 m-auto">
        <a class="btn btn-outline-agendar my-md-3 my-2 btn-block" href="mailto:contec@contabilcontec.com.br?subject=Questions">Agendar</a>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>